const Service = require('@ignitial/iio-services').Service
const config = require('./config')

var pino = require('pino')

pino = pino({
  name: 'iiost-service',
  safe: true,
  prettyPrint: { colorize: true }
})

class Iiost extends Service {
  constructor(options) {
    // set service name before calling super
    options.name = 'iiost'
    super(options)

    // wait for service to be ready in order to initialize it
    this._waitForPropertySet('_ready', true).then(() => {
      // initialize the service (register into Redis dico)
      this._init().then(() => {
        pino.info('Service ' + this._name + ' declaration done')
      }).catch(err => {
        pino.error('Initialization failed >', err)
        process.exit(1)
      })
    }).catch(err => {
      pino.error('Service not ready on time >', err)
      process.exit(1)
    })
  }

  // provides some services here
  oneServiceMethod(args) {
    return new Promise((resolve, reject) => {
      resolve({ somedata: 'some value' })
    })
  }
}

// instantiate service with its configuration
let migrant = new Iiost(config)
