FROM node:10-alpine

RUN npm install pm2 -g

RUN mkdir -p /opt && mkdir -p /opt/iiost

ADD . /opt/iiost

WORKDIR /opt/iiost

RUN mv vue.config.js.prod vue.config.js

RUN npm install && npm run build

CMD ["pm2-docker", "index.js"]
