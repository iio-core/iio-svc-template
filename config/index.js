
// HTTP server configuration
// -----------------------------------------------------------------------------
const IIOS_SERVER_PORT = process.env.NODE_ENV === 'production' ?
  8080 :
  process.env.IIOS_SERVER_PORT || 4093

const IIOS_SERVER_HOST = process.env.IIOS_SERVER_HOST || '127.0.0.1'
const IIOS_SERVER_PATH_TO_SERVE = process.env.IIOS_SERVER_PATH_TO_SERVE || './dist'

// REDIS configuration
// -----------------------------------------------------------------------------
const REDIS_HOST = process.env.REDIS_HOST || '127.0.0.1'
const REDIS_PORT = process.env.REDIS_PORT ? parseInt(process.env.REDIS_PORT) : 6379
const REDIS_DB = process.env.REDIS_DB || 0
let REDIS_SENTINELS

if (process.env.REDIS_SENTINELS) {
  REDIS_SENTINELS = []
  let sentinels = process.env.REDIS_SENTINELS.split(',')
  for (let s of sentinels) {
    REDIS_SENTINELS.push({ host: s.split(':')[0], port: s.split(':')[1] })
  }
}

// HTTP REST API key and context for restricted access
// -----------------------------------------------------------------------------
const REST_API_KEY = process.env.REST_API_KEY || '2bpukqziosbejet2k9duvpadajsfrm4u'
const REST_API_CONTEXT = process.env.REST_API_CONTEXT || '/api'

// Main configuration structure
// -----------------------------------------------------------------------------
module.exports = {
  /* service namesapce */
  namespace: process.env.IIOS_NAMESPACE || 'ignitialio',
  /* heartbeat */
  heartbeatPeriod: 5000,
  /* redis server connection */
  redis: {
    sentinels: REDIS_SENTINELS,
    host: REDIS_HOST,
    port: REDIS_PORT,
    db: REDIS_DB
  },
  /* HTTP server declaration */
  server: {
    /* server host */
    host: IIOS_SERVER_HOST,
    /* server port */
    port: IIOS_SERVER_PORT,
    /* path to statically serve (at least one asset for icons for example) */
    path: IIOS_SERVER_PATH_TO_SERVE
  },
  /* see connect-rest */
  rest: {
    context: REST_API_CONTEXT,
    apiKeys: [ REST_API_KEY ]
  },
  /* options published through discovery mechanism */
  publicOptions: {
    /* declares component injection */
    uiComponentInjection: true,
    /* service description */
    description: {
      /* service icon */
      icon: 'assets/iiost-64.png',
      /* Internationalization: see Ignitial.io Web App */
      i18n: {
        'My amazing component': [ 'Mon super composant' ],
        'Provides uber crazy services':  [
          'Fournit des services super hyper dingues'
        ]
      },
      /* eventually any other data */
      title: 'My amazing component',
      info: 'Provides uber crazy services'
    },
    /* https enabled */
    https: false,
    /* domain related public options: could be any JSON object*/
    myPublicOption: {
      name: 'iiost',
      description: 'Provides uber crazy services',
      icon: 'assets/iiost.png',
      component: 'iiost'
    }
  }
}
